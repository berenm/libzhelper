//
//  Task sink - design 2
//  Adds pub-sub flow to send kill signal to workers
//
#include "zmqh/socket.hpp"

#include <sys/time.h>
#include <iostream>

int main(int argc, char* argv[]) {
  //  Socket to receive messages on
  ::zmqh::pull receiver;
  receiver.bind("tcp://*:5558");

  //  Socket for worker control
  ::zmqh::pub controller;
  controller.bind("tcp://*:5559");

  //  Wait for start of batch
  receiver.recv();

  //  Start our clock now
  struct timeval tstart;
  gettimeofday(&tstart, nullptr);

  //  Process 100 confirmations
  int task_nbr;
  int total_msec = 0; //  Total calculated cost in msecs
  for (task_nbr = 0; task_nbr < 100; task_nbr++) {
    receiver.recv();

    if ((task_nbr / 10) * 10 == task_nbr) {
      ::std::cout << ":";
    } else {
      ::std::cout << ".";
    }

    ::std::cout.flush();
  }

  //  Calculate and report duration of batch
  struct timeval tend, tdiff;
  gettimeofday(&tend, nullptr);

  if (tend.tv_usec < tstart.tv_usec) {
    tdiff.tv_sec = tend.tv_sec - tstart.tv_sec - 1;
    tdiff.tv_usec = 1000000 + tend.tv_usec - tstart.tv_usec;
  } else {
    tdiff.tv_sec = tend.tv_sec - tstart.tv_sec;
    tdiff.tv_usec = tend.tv_usec - tstart.tv_usec;
  }
  total_msec = tdiff.tv_sec * 1000 + tdiff.tv_usec / 1000;
  ::std::cout << "Total elapsed time: " << total_msec << " msec" << ::std::endl;

  //  Send kill signal to workers
  controller.send(L"KILL");

  //  Finished
  sleep(1); //  Give 0MQ time to deliver
  return 0;
}
