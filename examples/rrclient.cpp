//
//  Hello World client
//  Connects REQ socket to tcp://localhost:5559
//  Sends "Hello" to server, expects "World" back
//
#include "zmqh/socket.hpp"

#include <iostream>

int main() {
  //  Socket to talk to server
  ::zmqh::req requester;
  requester.connect("tcp://localhost:5559");

  int request_nbr;
  for (request_nbr = 0; request_nbr != 10; request_nbr++) {
    requester.send(L"Hello");

    ::zmqh::string_t string;
    requester.recv(string);
    ::std::wcout << "Received reply " << request_nbr << " [" << string << "]" << ::std::endl;
  }

  return 0;
}
