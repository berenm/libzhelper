//
//  Least-recently used (LRU) queue device
//  Demonstrates use of the zmsg class
//
#include "zmqh/socket.hpp"
#include <iostream>

#define NBR_CLIENTS 10
#define NBR_WORKERS 3

//  A simple dequeue operation for queue implemented as array
#define DEQUEUE(q) memmove (&(q)[0], &(q)[1], sizeof (q) - sizeof (q [0]))

//  Basic request-reply client using REQ socket
//
static void* client_thread(void*) {
  ::zmqh::req client;
  client.set_id(); //  Makes tracing easier
  client.connect("ipc://frontend.ipc");

  //  Send request, get reply
  client.send(L"HELLO");
  ::zmqh::string_t reply = client.recv< ::zmqh::string_t > ();
  ::std::wcout << "Client: " << reply << ::std::endl;

  return nullptr;
}

//  Worker using REQ socket to do LRU routing
//
static void* worker_thread(void*) {
  ::zmqh::req worker;
  worker.set_id(); //  Makes tracing easier
  worker.connect("ipc://backend.ipc");

  //  Tell broker we're ready for work
  worker.send("READY");

  while (true) {
    ::zmqh::string_t message = worker.recv< ::zmqh::string_t > ();
    ::std::wcout << "Worker: " << message << ::std::endl;

    worker.send(L"OK");
  }

  return nullptr;
}

int main(int argc, char* argv[]) {
  //  Prepare our context and sockets
  ::zmqh::xrep frontend;
  ::zmqh::xrep backend;
  frontend.bind("ipc://frontend.ipc");
  backend.bind("ipc://backend.ipc");

  int client_nbr;
  for (client_nbr = 0; client_nbr < NBR_CLIENTS; client_nbr++) {
    pthread_t client;
    pthread_create(&client, nullptr, client_thread, context);
  }
  int worker_nbr;
  for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++) {
    pthread_t worker;
    pthread_create(&worker, nullptr, worker_thread, context);
  }

  //  Logic of LRU loop
  //  - Poll backend always, frontend only if 1+ worker ready
  //  - If worker replies, queue worker as ready and forward reply
  //    to client if necessary
  //  - If client requests, pop next worker and send request to it

  //  Queue of available workers
  ::std::queue< ::zmqh::binary_t > worker_queue;

  while (true) {
    //  Initialize poll set
    zmq_pollitem_t items[] = {
    //  Always poll for worker activity on backend
                                { backend, 0, ZMQ_POLLIN, 0 },
                               //  Poll front-end only if we have available workers
                                { frontend, 0, ZMQ_POLLIN, 0 } };
    if (worker_queue.size() > 0) {
      zmq_poll(items, 2, -1);
    } else {
      zmq_poll(items, 1, -1);
    }

    //  Handle worker activity on backend
    if (items[0].revents & ZMQ_POLLIN) {
      zmsg_t *zmsg = zmsg_recv(backend);
      //  Use worker address for LRU routing
      worker_queue[available_workers++] = zmsg_unwrap(zmsg);

      //  Forward message to client if it's not a READY
      if (strcmp(zmsg_address(zmsg), "READY") == 0)
        zmsg_destroy(&zmsg);
      else {
        zmsg_send(&zmsg, frontend);
        if (--client_nbr == 0)
          break; //  Exit after N messages
      }
    }

    if (items[1].revents & ZMQ_POLLIN) {
      //  Now get next client request, route to next worker
      zmsg_t *zmsg = zmsg_recv(frontend);
      zmsg_wrap(zmsg, worker_queue[0], "");
      zmsg_send(&zmsg, backend);

      //  Dequeue and drop the next worker address
      free(worker_queue[0]);
      DEQUEUE (worker_queue);
      available_workers--;
    }
  }
  sleep(1);

  return 0;
}
