//
//  Synchronized subscriber
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

#include <iostream>

int main(int argc, char* argv[]) {
  //  First, connect our subscriber socket
  ::zmqh::sub subscriber;
  subscriber.connect("tcp://localhost:5561");
  subscriber.set_option< ::zmqh::subscribe > ("");

  //  Second, synchronize with publisher
  ::zmqh::req syncclient;
  syncclient.connect("tcp://localhost:5562");

  //  - send a synchronization request
  syncclient.send();

  //  - wait for synchronization reply
  syncclient.recv();

  //  Third, get our updates and report how many we got
  int update_nbr = 0;
  while (true) {
    ::zmqh::string_t string;
    subscriber.recv(string);

    if (string.compare(L"END") == 0) {
      break;
    }

    update_nbr++;
  }
  ::std::cout << "Received " << update_nbr << " updates" << ::std::endl;

  return 0;
}
