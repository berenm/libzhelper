//
//  Weather proxy device
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

int main(int argc, char* argv[]) {
  //  This is where the weather server sits
  ::zmqh::sub frontend;
  frontend.connect("tcp://192.168.55.210:5556");

  //  This is our public endpoint for subscribers
  ::zmqh::pub backend;
  backend.bind("tcp://10.1.1.0:8100");

  //  Subscribe on everything
  frontend.set_option< ::zmqh::subscribe > ("");

  //  Shunt messages out to our own subscribers
  while (true) {
    while (true) {
      ::zmqh::message_t message;
      frontend.recv(message);

      bool more = frontend.option< ::zmqh::receive_more > ();
      backend.send(message, more ? ZMQ_SNDMORE : 0);

      if (!more)
        break; //  Last message part
    }
  }

  return 0;
}
