//
//  Cross-connected XREP sockets addressing each other
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

int main() {
  ::zmqh::xrep worker;
  worker.set_option< ::zmqh::identity > ("WORKER");
  worker.bind("ipc://rtrouter.ipc");

  ::zmqh::xrep server;
  server.set_option< ::zmqh::identity > ("SERVER");
  server.connect("ipc://rtrouter.ipc");

  sleep(1);
  server.send_more(L"WORKER");
  server.send_more();
  server.send(L"send to worker");

  worker.dump();

  worker.send_more(L"SERVER");
  worker.send_more();
  worker.send(L"send to server");

  server.dump();

  return 0;
}
