//
//  Multithreaded relay
//
#include "zmqh/socket.hpp"

#include <iostream>

static void* step1(void*) {
  //  Signal downstream to step 2
  ::zmqh::pair sender;
  sender.connect("inproc://step2");
  sender.send();

  return nullptr;
}

static void* step2(void*) {
  //  Bind to inproc: endpoint, then start upstream thread
  ::zmqh::pair receiver;
  receiver.bind("inproc://step2");
  pthread_t thread;
  pthread_create(&thread, nullptr, step1, nullptr);

  //  Wait for signal
  receiver.recv();

  //  Signal downstream to step 3
  ::zmqh::pair sender;
  sender.connect("inproc://step3");
  sender.send();

  return nullptr;
}

int main() {
  //  Bind to inproc: endpoint, then start upstream thread
  ::zmqh::pair receiver;
  receiver.bind("inproc://step3");
  pthread_t thread;
  pthread_create(&thread, nullptr, step2, nullptr);

  //  Wait for signal
  receiver.recv();

  ::std::cout << "Test successful!" << ::std::endl;
  return 0;
}
