//
//  Custom routing Router to Dealer (XREP to XREQ)
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

#include <iostream>

#define within(num) (int) ((float) (num) * random () / (RAND_MAX + 1.0))

//  We have two workers, here we copy the code, normally these would
//  run on different boxes...
//
void* worker_a(void* context) {
  ::zmqh::xreq worker;
  worker.set_option< ::zmqh::identity > ("A");
  worker.connect("ipc://routing.ipc");

  int total = 0;
  while (true) {
    //  We receive one part, with the workload
    ::zmqh::string_t request;
    worker.recv(request);
    int finished = (request.compare(L"END") == 0);

    if (finished) {
      ::std::cout << "A received: " << total << ::std::endl;
      break;
    }

    total++;
  }

  return nullptr;
}

void* worker_b(void* context) {
  ::zmqh::xreq worker;
  worker.set_option< ::zmqh::identity > ("B");
  worker.connect("ipc://routing.ipc");

  int total = 0;
  while (true) {
    //  We receive one part, with the workload
    ::zmqh::string_t request;
    worker.recv(request);
    int finished = (request.compare(L"END") == 0);

    if (finished) {
      ::std::cout << "B received: " << total << ::std::endl;
      break;
    }

    total++;
  }

  return nullptr;
}

int main() {
  ::zmqh::xrep client;
  client.bind("ipc://routing.ipc");

  pthread_t worker;
  pthread_create(&worker, nullptr, worker_a, nullptr);
  pthread_create(&worker, nullptr, worker_b, nullptr);

  //  Wait for threads to stabilize
  sleep(1);

  //  Send 10 tasks scattered to A twice as often as B
  int task_nbr;
  srandom((unsigned) time(nullptr));

  for (task_nbr = 0; task_nbr < 10; task_nbr++) {
    //  Send two message parts, first the address...
    if (within(3) > 0) {
      client.send_more(L"A");
    } else {
      client.send_more(L"B");
    }

    //  And then the workload
    client.send(L"This is the workload");
  }

  client.send_more(L"A");
  client.send(L"END");

  client.send_more(L"B");
  client.send(L"END");

  sleep(1); //  Give 0MQ/2.0.x time to flush output

  return 0;
}
