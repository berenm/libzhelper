//
//  Simple message queuing broker
//  Same as request-reply broker but using QUEUE device
//
#include "zmqh/socket.hpp"
#include "zmqh/device.hpp"

int main(int argc, char* argv[]) {
  //  Socket facing clients
  ::zmqh::xrep frontend;
  frontend.bind("tcp://*:5559");

  //  Socket facing services
  ::zmqh::xreq backend;
  backend.bind("tcp://*:5560");

  //  Start built-in device
  ::zmqh::queue(frontend, backend);

  return 0;
}
