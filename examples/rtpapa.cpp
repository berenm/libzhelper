//
//  Custom routing Router to Papa (XREP to REP)
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

//  We will do this all in one thread to emphasize the sequence
//  of events...
int main() {
  ::zmqh::xrep client;
  client.bind("ipc://routing.ipc");

  ::zmqh::rep worker;
  worker.set_option< ::zmqh::identity > ("A");
  worker.connect("ipc://routing.ipc");

  //  Wait for sockets to stabilize
  sleep(1);

  //  Send papa address, address stack, empty part, and request
  client.send_more(L"A");
  client.send_more(L"address 3");
  client.send_more(L"address 2");
  client.send_more(L"address 1");
  client.send_more();
  client.send(L"This is the workload");

  //  Worker should get just the workload
  worker.dump();

  //  We don't play with envelopes in the worker
  worker.send(L"This is the reply");

  //  Now dump what we got off the XREP socket...
  client.dump();

  return 0;
}
