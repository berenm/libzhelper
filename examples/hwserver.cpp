//
//  Hello World server
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//

#include "zmqh/zmqh.hpp"
#include "zmqh/socket.hpp"
#include "zmqh/message.hpp"

#include <iostream>

int main() {
  //  Socket to talk to clients
  ::zmqh::rep responder;
  responder.bind("tcp://*:5555");

  while (true) {
    //  Wait for next request from client
    ::zmqh::string_t request;
    responder.recv(request);
    ::std::wcout << "Received request: [" << request << "]" << ::std::endl;

    //  Do some 'work'
    sleep(1);

    //  Send reply back to client
    responder.send(L"World");
  }

  return 0;
}
