//
//  Task ventilator
//  Binds PUSH socket to tcp://localhost:5557
//  Sends batch of tasks to workers via that socket
//
#include "zmqh/socket.hpp"

#include <iostream>

#define within(num) (int) ((float) (num) * random () / (RAND_MAX + 1.0))

int main(int argc, char* argv[]) {
  //  Socket to send messages on
  ::zmqh::push sender;
  sender.bind("tcp://*:5557");

  ::std::cout << "Press Enter when the workers are ready: ";
  ::std::cin.get();
  ::std::cout << "Sending tasks to workers..." << ::std::endl;

  //  The first message is "0" and signals start of batch
  sender.send(L"0");

  //  Initialize random number generator
  srandom((unsigned) time(nullptr));

  //  Send 100 tasks
  int task_nbr;
  int total_msec = 0; //  Total expected cost in msecs
  for (task_nbr = 0; task_nbr < 100; task_nbr++) {
    int workload;
    //  Random workload from 1 to 100msecs
    workload = within(100) + 1;
    total_msec += workload;

    ::std::wostringstream stream;
    stream << workload;

    sender.send(stream.str());
  }

  ::std::cout << "Total expected cost: " << total_msec << "msec" << ::std::endl;
  sleep(1); //  Give 0MQ time to deliver
  return 0;
}
