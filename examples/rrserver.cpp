//
//  Hello World server
//  Connects REP socket to tcp://*:5560
//  Expects "Hello" from client, replies with "World"
//
#include "zmqh/socket.hpp"

#include <iostream>

int main() {
  //  Socket to talk to clients
  ::zmqh::rep responder;
  responder.connect("tcp://localhost:5560");

  while (true) {
    //  Wait for next request from client
    ::zmqh::string_t string;
    responder.recv(string);
    ::std::wcout << "Received request: [" << string << "]" << ::std::endl;

    //  Do some 'work'
    sleep(1);

    //  Send reply back to client
    responder.send(L"World");
  }

  return 0;
}
