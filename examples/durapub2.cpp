//
//  Publisher for durable subscriber
//

#include "zmqh/zmqh.hpp"
#include "zmqh/socket.hpp"
#include "zmqh/message.hpp"
#include "zmqh/option.hpp"

#include <sstream>

int main() {
  //  Subscriber tells us when it's ready here
  ::zmqh::pull sync;
  sync.bind("tcp://*:5564");

  //  We send updates via this socket
  ::zmqh::pub publisher;
  publisher.bind("tcp://*:5565");

  //  Prevent publisher overflow from slow subscribers
  publisher.set_option< ::zmqh::high_water_mark_size > (1);

  //  Specify swap space in bytes, this covers all subscribers
  publisher.set_option< ::zmqh::swap_size > (25000000);

  //  Wait for synchronization request
  sync.recv();

  //  Now broadcast exactly 10 updates with pause
  for (int update_nbr = 0; update_nbr < 10; update_nbr++) {
    ::std::wstringstream stream;
    stream << "Update " << update_nbr;
    publisher.send(stream.str());
    sleep(1);
  }
  publisher.send(L"END");

  sleep(1); //  Give 0MQ/2.0.x time to flush output
  return 0;
}
