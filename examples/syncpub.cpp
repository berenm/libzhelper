//
//  Synchronized publisher
//
#include "zmqh/socket.hpp"

//  We wait for 10 subscribers
#define SUBSCRIBERS_EXPECTED  10

int main() {
  //  Socket to talk to clients
  ::zmqh::pub publisher;
  publisher.bind("tcp://*:5561");

  //  Socket to receive signals
  ::zmqh::rep syncservice;
  syncservice.bind("tcp://*:5562");

  //  Get synchronization from subscribers
  int subscribers = 0;
  while (subscribers < SUBSCRIBERS_EXPECTED) {
    //  - wait for synchronization request
    syncservice.recv();
    //  - send synchronization reply
    syncservice.send();
    subscribers++;
  }

  //  Now broadcast exactly 1M updates followed by END
  int update_nbr;
  for (update_nbr = 0; update_nbr < 1000000; update_nbr++)
    publisher.send(L"Rhubarb");

  publisher.send(L"END");

  sleep(1); //  Give 0MQ/2.0.x time to flush output
  return 0;
}
