//
//  Demonstrate identities as used by the request-reply pattern.  Run this
//  program by itself.  Note that the utility functions s_ are provided by
//  zhelpers.h.  It gets boring for everyone to keep repeating this code.
//

#include "zmqh/zmqh.hpp"
#include "zmqh/socket.hpp"
#include "zmqh/message.hpp"
#include "zmqh/error.hpp"
#include "zmqh/option.hpp"

#include <iostream>

int main() {
  //  First allow 0MQ to set the identity
  ::zmqh::xrep sink;
  sink.bind("inproc://example");

  ::zmqh::req anonymous;
  anonymous.connect("inproc://example");
  anonymous.send(L"XREP uses a generated UUID");
  sink.dump();

  ::zmqh::req identified;
  identified.set_option< ::zmqh::identity > ("Hello");
  identified.connect("inproc://example");
  identified.send(L"XREP socket uses REQ's socket identity");
  sink.dump();

  return 0;
}
