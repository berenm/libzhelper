//
//  Weather update client
//  Connects SUB socket to tcp://localhost:5556
//  Collects weather updates and finds avg temp in zipcode
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

#include <iostream>

int main(int argc, char* argv[]) {
  //  Socket to talk to server
  ::std::cout << "Collecting updates from weather server..." << ::std::endl;
  ::zmqh::sub subscriber;
  subscriber.connect("tcp://localhost:5556");

  //  Subscribe to zipcode, default is NYC, 10001
  ::std::string filter = (argc > 1) ? ::std::string(argv[1]) : "10001";
  subscriber.set_option< ::zmqh::subscribe > (filter);

  //  Process 100 updates
  int update_nbr;
  long total_temp = 0;
  for (update_nbr = 0; update_nbr < 100; update_nbr++) {
    ::std::wstring string;
    subscriber.recv(string);

    int zipcode, temperature, relhumidity;
    ::std::wistringstream stream(string);
    stream >> zipcode;
    stream >> temperature;
    stream >> relhumidity;

    total_temp += temperature;
  }

  ::std::cout << "Average temperature for zipcode '" << filter << "' was " << (int) (total_temp / update_nbr)
      << ::std::endl;

  return 0;
}
