//
//  Weather update server
//  Binds PUB socket to tcp://*:5556
//  Publishes random weather updates
//
#include "zmqh/socket.hpp"

#include <iomanip>

#define within(num) (int) ((float) (num) * random () / (RAND_MAX + 1.0))

int main() {
  //  Prepare our context and publisher
  ::zmqh::pub publisher;
  publisher.bind("tcp://*:5556");
  publisher.bind("ipc://weather.ipc");

  //  Initialize random number generator
  srandom((unsigned) time(nullptr));
  while (true) {
    //  Get values that will fool the boss
    int zipcode, temperature, relhumidity;
    zipcode = within(100000);
    temperature = within (215) - 80;
    relhumidity = within (50) + 10;

    //  Send message to all subscribers
    ::std::wstringstream stream;
    stream << ::std::setfill(L'0') << ::std::setw(5) << zipcode;
    stream << L' ';
    stream << temperature;
    stream << L' ';
    stream << relhumidity;

    publisher.send(stream.str());
  }

  return 0;
}
