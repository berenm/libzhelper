//
//  Task worker
//  Connects PULL socket to tcp://localhost:5557
//  Collects workloads from ventilator via that socket
//  Connects PUSH socket to tcp://localhost:5558
//  Sends results to sink via that socket
//
#include "zmqh/socket.hpp"

#include <iostream>

int main(int argc, char* argv[]) {
  //  Socket to receive messages on
  ::zmqh::pull receiver;
  receiver.connect("tcp://localhost:5557");

  //  Socket to send messages to
  ::zmqh::push sender;
  sender.connect("tcp://localhost:5558");

  //  Process tasks forever
  while (true) {
    ::zmqh::binary_t string;
    receiver.recv(string);
    struct timespec t;
    t.tv_sec = 0;
    t.tv_nsec = atoi(reinterpret_cast< const char* > (string.c_str())) * 1000000;

    //  Simple progress indicator for the viewer
    ::std::wcout.flush();
    ::std::wcout << ::zmqh::to_string(string) << ".";

    //  Do the work
    nanosleep(&t, nullptr);

    //  Send results to sink
    sender.send();
  }

  return 0;
}
