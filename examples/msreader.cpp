//
//  Reading from multiple sockets
//  This version uses a simple recv loop
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

int main(int argc, char* argv[]) {
  //  Connect to task ventilator
  ::zmqh::pull receiver;
  receiver.connect("tcp://localhost:5557");

  //  Connect to weather server
  ::zmqh::sub subscriber;
  subscriber.connect("tcp://localhost:5556");
  subscriber.set_option< ::zmqh::subscribe > ("10001 ");

  //  Process messages from both sockets
  //  We prioritize traffic from the task ventilator
  while (true) {
    //  Process any waiting tasks
    bool success = false;
    do {
      ::zmqh::message_t task;
      success = receiver.recv< ::zmqh::mode::NO_BLOCK > (task);

      if (success) {
        // process task
      }
    } while (success);

    //  Process any waiting weather updates
    success = false;
    do {
      ::zmqh::message_t update;
      success = subscriber.recv< ::zmqh::mode::NO_BLOCK > (update);

      if (success) {
        // process update
      }
    } while (success);

    //  No activity, so sleep for 1 msec
    struct timespec t;
    t.tv_sec = 0;
    t.tv_nsec = 1000000;
    nanosleep(&t, nullptr);
  }

  return 0;
}
