//
//  Least-recently used (LRU) queue device
//  Clients and workers are shown here in-process
//

#include "zmqh/zmqh.hpp"
#include "zmqh/socket.hpp"
#include "zmqh/socket_stream.hpp"
#include "zmqh/message.hpp"
#include "zmqh/multipart.hpp"
#include "zmqh/error.hpp"

#include <iostream>
#include <queue>

#define NBR_CLIENTS 10
#define NBR_WORKERS 3

//  Basic request-reply client using REQ socket
//
static void*
client_thread(void* context) {
  ::zmqh::req client;
  client.set_id(); //  Makes tracing easier
  client.connect("ipc://frontend.ipc");

  //  Send request, get reply
  client.send(L"HELLO");
  ::zmqh::string_t reply;
  client.recv(reply);
  ::std::wcout << reply << ::std::endl;

  return nullptr;
}

//  Worker using REQ socket to do LRU routing
//
static void*
worker_thread(void* context) {
  ::zmqh::req worker;
  worker.set_id(); //  Makes tracing easier
  worker.connect("ipc://backend.ipc");

  //  Tell broker we're ready for work
  worker.send(L"READY");

  while (true) {
    //  Read and save all frames until we get an empty frame
    //  In this example there is only 1 but it could be more

    //  Get request, send reply
    ::zmqh::binary_t client_address;
    ::zmqh::string_t request;
    worker.recv(client_address);
    worker.recv(request);

    ::std::wcout << "Worker: " << request << ::std::endl;

    worker.send_more(client_address);
    worker.send(L"OK");
  }
  return nullptr;
}

int main(int argc, char* argv[]) {
  //  Prepare our context and sockets
  ::zmqh::xrep frontend;
  ::zmqh::xrep backend;

  frontend.bind("ipc://frontend.ipc");
  backend.bind("ipc://backend.ipc");

  int client_nbr;
  for (client_nbr = 0; client_nbr < NBR_CLIENTS; client_nbr++) {
    pthread_t client;
    pthread_create(&client, nullptr, client_thread, nullptr);
  }

  int worker_nbr;
  for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++) {
    pthread_t worker;
    pthread_create(&worker, nullptr, worker_thread, nullptr);
  }

  //  Logic of LRU loop
  //  - Poll backend always, frontend only if 1+ worker ready
  //  - If worker replies, queue worker as ready and forward reply
  //    to client if necessary
  //  - If client requests, pop next worker and send request to it

  //  Queue of available workers
  ::std::queue< ::zmqh::binary_t > worker_queue;

  while (true) {
    //  Initialize poll set
    zmq_pollitem_t items[] = {
    //  Always poll for worker activity on backend
                                { backend, 0, ZMQ_POLLIN, 0 },
                               //  Poll front-end only if we have available workers
                                { frontend, 0, ZMQ_POLLIN, 0 } };
    if (worker_queue.size() > 0) {
      zmq_poll(items, 2, -1);
    } else {
      zmq_poll(items, 1, -1);
    }

    //  Handle worker activity on backend
    if (items[0].revents & ZMQ_POLLIN) {
      //  Queue worker address for LRU routing
      ::zmqh::binary_t worker_address;
      ::zmqh::string_t body;

      backend.recv(worker_address);
      backend.recv();
      backend.recv(body);

      worker_queue.push(worker_address);

      //  If client reply, send rest back to frontend
      if (body.compare(L"READY") != 0) {
        ::zmqh::binary_t client_address = ::zmqh::to_binary(body);
        ::zmqh::string_t reply;
        backend.recv(reply);

        frontend.send_more(client_address);
        frontend.send_more();
        frontend.send(reply);

        if (--client_nbr == 0) {
          break; //  Exit after N messages
        }
      }
    }

    if (items[1].revents & ZMQ_POLLIN) {
      //  Now get next client request, route to LRU worker
      //  Client request is [address][empty][request]
      ::zmqh::binary_t client_address;
      ::zmqh::string_t body;

      frontend.recv(client_address);
      frontend.recv();
      frontend.recv(body);

      backend.send_more(worker_queue.front());
      backend.send_more();
      backend.send_more(client_address);
      backend.send(body);

      //  Dequeue and drop the next worker address
      worker_queue.pop();
    }
  }

  sleep(1);
  return 0;
}
