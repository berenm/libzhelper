//
//  Hello World client
//  Connects REQ socket to tcp://localhost:5555
//  Sends "Hello" to server, expects "World" back
//

#include "zmqh/zmqh.hpp"
#include "zmqh/socket.hpp"
#include "zmqh/message.hpp"

#include <iostream>

int main() {
  //  Socket to talk to server
  ::std::wcout << "Connecting to hello world server..." << ::std::endl;
  ::zmqh::req requester;
  requester.connect("tcp://localhost:5555");

  for (int request_nbr = 0; request_nbr != 10; request_nbr++) {
    ::std::wcout << "Sending request " << request_nbr << "..." << ::std::endl;
    requester.send(L"Hello");

    ::zmqh::string_t reply;
    requester.recv(reply);
    ::std::wcout << "Received reply " << request_nbr << ": [" << reply << "]" << ::std::endl;
  }

  return 0;
}
