//
//  Multithreaded Hello World server
//
#include "zmqh/socket.hpp"
#include "zmqh/device.hpp"

#include <iostream>

static void* worker_routine(void* context) {
  //  Socket to talk to dispatcher
  ::zmqh::rep receiver;
  receiver.connect("inproc://workers");

  while (true) {
    ::zmqh::string_t string;
    receiver.recv(string);
    ::std::wcout << "Received request: [" << string << "]" << ::std::endl;
    //  Do some 'work'
    sleep(1);
    //  Send reply back to client
    receiver.send(L"World");
  }

  return nullptr;
}

int main() {
  //  Socket to talk to clients
  ::zmqh::xrep clients;
  clients.bind("tcp://*:5555");

  //  Socket to talk to workers
  ::zmqh::xreq workers;
  workers.bind("inproc://workers");

  //  Launch pool of worker threads
  int thread_nbr;
  for (thread_nbr = 0; thread_nbr != 5; thread_nbr++) {
    pthread_t worker;
    pthread_create(&worker, nullptr, worker_routine, nullptr);
  }

  //  Connect work threads to client threads via a queue
  ::zmqh::queue(clients, workers);

  return 0;
}
