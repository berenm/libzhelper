//
//  Task worker - design 2
//  Adds pub-sub flow to receive and respond to kill signal
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"
#include <iostream>

int main(int argc, char* argv[]) {
  //  Socket to receive messages on
  ::zmqh::pull receiver;
  receiver.connect("tcp://localhost:5557");

  //  Socket to send messages to
  ::zmqh::push sender;
  sender.connect("tcp://localhost:5558");

  //  Socket for control input
  ::zmqh::sub controller;
  controller.connect("tcp://localhost:5559");
  controller.set_option< ::zmqh::subscribe > ("");

  //  Process messages from receiver and controller
  zmq_pollitem_t items[] = { { receiver, 0, ZMQ_POLLIN, 0 }, { controller, 0, ZMQ_POLLIN, 0 } };
  //  Process messages from both sockets
  while (true) {
    zmq_poll(items, 2, -1);
    if (items[0].revents & ZMQ_POLLIN) {
      ::zmqh::string_t message;
      receiver.recv(message);
      ::std::wistringstream stream(message);

      //  Process task
      int workload; //  Workload in msecs
      stream >> workload;

      struct timespec t;
      t.tv_sec = 0;
      t.tv_nsec = workload * 1000000;

      //  Do the work
      nanosleep(&t, nullptr);

      //  Send results to sink
      sender.send(message);

      //  Simple progress indicator for the viewer
      ::std::cout << ".";
      ::std::cout.flush();
    }

    //  Any waiting controller command acts as 'KILL'
    if (items[1].revents & ZMQ_POLLIN) {
      break; //  Exit loop
    }
  }
  //  Finished

  return 0;
}
