//
//  Simple request-reply broker
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

int main(int argc, char* argv[]) {
  //  Prepare our context and sockets
  ::zmqh::xrep frontend;
  ::zmqh::xreq backend;
  frontend.bind("tcp://*:5559");
  backend.bind("tcp://*:5560");

  //  Initialize poll set
  zmq_pollitem_t items[] = { { frontend, 0, ZMQ_POLLIN, 0 }, { backend, 0, ZMQ_POLLIN, 0 } };
  //  Switch messages between sockets
  while (true) {
    zmq_poll(items, 2, -1);
    if (items[0].revents & ZMQ_POLLIN) {
      while (true) {
        //  Process all parts of the message
        ::zmqh::binary_t message;
        frontend.recv(message);

        bool more = frontend.option< ::zmqh::receive_more > ();
        if (more) {
          backend.send_more(message);
        } else {
          backend.send(message);
          break;
        }
      }
    }

    if (items[1].revents & ZMQ_POLLIN) {
      while (true) {
        //  Process all parts of the message
        ::zmqh::binary_t message;
        backend.recv(message);

        bool more = backend.option< ::zmqh::receive_more > ();
        if (more) {
          frontend.send_more(message);
        } else {
          frontend.send(message);
          break;
        }
      }
    }
  }

  return 0;
}
