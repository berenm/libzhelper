//
//  Pubsub envelope subscriber
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

#include <iostream>

int main() {
  //  Prepare our context and subscriber
  ::zmqh::sub subscriber;
  subscriber.connect("tcp://localhost:5563");
  subscriber.set_option< ::zmqh::subscribe > ("B");

  while (true) {
    ::zmqh::binary_t address;
    ::zmqh::string_t contents;

    //  Read envelope with address
    subscriber.recv(address);
    //  Read message contents
    subscriber.recv(contents);

    ::std::wcout << L"[" << ::zmqh::to_string(address) << L"] " << contents << ::std::endl;
  }

  return 0;
}
