//
//  Reading from multiple sockets
//  This version uses zmq_poll()
//
#include "zmqh/socket.hpp"
#include "zmqh/option.hpp"

int main(int argc, char* argv[]) {
  //  Connect to task ventilator
  ::zmqh::pull receiver;
  receiver.connect("tcp://localhost:5557");

  //  Connect to weather server
  ::zmqh::sub subscriber;
  subscriber.connect("tcp://localhost:5556");
  subscriber.set_option< ::zmqh::subscribe > ("10001 ");

  //  Initialize poll set
  zmq_pollitem_t items[] = { { receiver, 0, ZMQ_POLLIN, 0 }, { subscriber, 0, ZMQ_POLLIN, 0 } };
  //  Process messages from both sockets
  while (true) {
    zmq_poll(items, 2, -1);

    if (items[0].revents & ZMQ_POLLIN) {
      receiver.recv();
    }

    if (items[1].revents & ZMQ_POLLIN) {
      subscriber.recv();
    }
  }

  return 0;
}
