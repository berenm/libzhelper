//
//  Durable subscriber
//

#include "zmqh/zmqh.hpp"
#include "zmqh/socket.hpp"
#include "zmqh/message.hpp"
#include "zmqh/option.hpp"

#include <iostream>

int main(int argc, char* argv[]) {
  //  Connect our subscriber socket
  ::zmqh::sub subscriber;
  subscriber.set_option< ::zmqh::identity > ("Hello");
  subscriber.set_option< ::zmqh::subscribe > ("");
  subscriber.connect("tcp://localhost:5565");

  //  Synchronize with publisher
  ::zmqh::push sync;
  sync.connect("tcp://localhost:5564");
  sync.send();

  //  Get updates, expect random Ctrl-C death
  while (true) {
    ::zmqh::string_t string;
    subscriber.recv(string);

    ::std::wcout << string << ::std::endl;
    if (string.compare(L"END") == 0) {
      break;
    }
  }
  return 0;
}
