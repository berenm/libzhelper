//
//  Custom routing Router to Mama (XREP to REQ)
//
#include "zmqh/socket.hpp"

#include <iostream>

#define within(num) (int) ((float) (num) * random () / (RAND_MAX + 1.0))

#define NBR_WORKERS 10

static void*
worker_thread(void* context) {
  ::zmqh::req worker;

  //  We use a string identity for ease here
  worker.set_id();
  worker.connect("ipc://routing.ipc");

  int total = 0;
  while (true) {
    //  Tell the router we're ready for work
    worker.send(L"ready");

    //  Get workload from router, until finished
    ::zmqh::string_t workload;
    worker.recv(workload);
    int finished = (workload.compare(L"END") == 0);
    if (finished) {
      ::std::cout << "Processed: " << total << " tasks" << ::std::endl;
      break;
    }
    total++;

    //  Do some random work
    struct timespec t;
    t.tv_sec = 0;
    t.tv_nsec = within(100000000) + 1;
    nanosleep(&t, nullptr);
  }
  return (nullptr);
}

int main() {
  ::zmqh::xrep client;
  client.bind("ipc://routing.ipc");
  srandom((unsigned) time(nullptr));

  int worker_nbr;
  for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++) {
    pthread_t worker;
    pthread_create(&worker, nullptr, worker_thread, nullptr);
  }

  int task_nbr;
  for (task_nbr = 0; task_nbr < NBR_WORKERS * 10; task_nbr++) {
    //  LRU worker is next waiting in queue
    ::zmqh::binary_t address;
    client.recv(address);
    client.recv();
    client.recv();

    client.send_more(address);
    client.send_more();
    client.send(L"This is the workload");
  }

  //  Now ask mamas to shut down and report their results
  for (worker_nbr = 0; worker_nbr < NBR_WORKERS; worker_nbr++) {
    ::zmqh::binary_t address;
    client.recv(address);
    client.recv();
    client.recv();

    client.send_more(address);
    client.send_more();
    client.send(L"END");
  }

  sleep(1); //  Give 0MQ/2.0.x time to flush output
  return 0;
}
