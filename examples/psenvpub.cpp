//
//  Pubsub envelope publisher
//  Note that the zhelpers.h file also provides s_sendmore
//
#include "zmqh/socket.hpp"

int main() {
  //  Prepare our context and publisher
  ::zmqh::pub publisher;
  publisher.bind("tcp://*:5563");

  while (true) {
    //  Write two messages, each with an envelope and content
    publisher.send_more(L"A");
    publisher.send(L"We don't want to see this");

    publisher.send_more(L"B");
    publisher.send(L"We would like to see this");

    sleep(1);
  }

  return 0;
}
