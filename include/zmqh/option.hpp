/**
 * @file
 * @date 9 nov. 2010
 * @todo comment
 */

#ifndef ZMQH_OPTION_HPP_
#define ZMQH_OPTION_HPP_

#include "zmqh/zmqh.hpp"
#include "zmqh/option_fwd.hpp"

namespace zmqh {
  namespace socket {

    template< typename value_type_t >
    struct option_setter {
        static void set(socket_base const& socket_in, socket_option const socket_option_in, value_type_t const value_in)
            throw (::zmqh::error_t) {
          if (zmq_setsockopt(socket_in, static_cast< int > (socket_option_in), &value_in, sizeof(value_in)) != 0) {
            throw ::zmqh::error_t();
          }
        }
    };

    template< >
    struct option_setter< bool > {
        static void set(socket_base const& socket_in, socket_option const socket_option_in, bool const value_in) {
          option_setter< int >::set(socket_in, socket_option_in, (value_in ? 1 : 0));
        }
    };

    template< >
    struct option_setter< binary_t > {
        static void set(socket_base const& socket_in, socket_option const socket_option_in, binary_t const& value_in)
            throw (::zmqh::error_t) {
          if (zmq_setsockopt(socket_in, static_cast< int > (socket_option_in), value_in.data(), value_in.size()) != 0) {
            throw ::zmqh::error_t();
          }
        }

        static void set(socket_base const& socket_in, socket_option const socket_option_in, string_t const& value_in) {
          set(socket_in, socket_option_in, to_binary(value_in));
        }

        static void set(socket_base const& socket_in,
                        socket_option const socket_option_in,
                        ::std::string const& value_in) {
          set(socket_in, socket_option_in, to_binary(value_in));
        }

        static void set(socket_base const& socket_in, socket_option const socket_option_in, char const* value_in) {
          set(socket_in, socket_option_in, to_binary(value_in));
        }

        static void set(socket_base const& socket_in, socket_option const socket_option_in, wchar_t const* value_in) {
          set(socket_in, socket_option_in, to_binary(value_in));
        }

        template< typename value_type_t >
        static void set(socket_base const& socket_in, socket_option const socket_option_in, value_type_t const value_in) {
          option_setter< value_type_t >::set(socket_in, socket_option_in, value_in);
        }
    };

    template< typename value_type_t >
    struct option_getter {
        static value_type_t get(socket_base const& socket_in, socket_option const socket_option_in)
            throw (::zmqh::error_t) {
          value_type_t value_out;
          size_t size = sizeof(value_out);

          if (zmq_getsockopt(socket_in, static_cast< int > (socket_option_in), &value_out, &size) != 0) {
            throw ::zmqh::error_t();
          }

          return value_out;
        }
    };

    template< >
    struct option_getter< bool > {
        static bool get(socket_base const& socket_in, socket_option const socket_option_in) {
          return option_getter< int64_t >::get(socket_in, socket_option_in) != 0;
        }
    };

    template< >
    struct option_getter< binary_t > {
        static binary_t get(socket_base const& socket_in, socket_option const socket_option_in) throw (::zmqh::error_t) {
          binary_t::value_type value[256];
          size_t size;

          if (zmq_getsockopt(socket_in, static_cast< int > (socket_option_in), value, &size) != 0) {
            throw ::zmqh::error_t();
          }

          return binary_t(value, size);
        }

        template< typename value_type_t >
        static value_type_t get(socket_base const& socket_in, socket_option const socket_option_in) {
          return option_getter< value_type_t >::get(socket_in, socket_option_in);
        }
    };

    template< socket_option socket_option_t, typename value_type_t >
    struct read_option: public option_getter< value_type_t > {
        typedef value_type_t value_type;

        static value_type_t get(socket_base const& socket_in) {
          return option_getter< value_type_t >::get(socket_in, socket_option_t);
        }
    };

    template< socket_option socket_option_t, typename value_type_t >
    struct write_option: public option_setter< value_type_t > {
        typedef value_type_t value_type;

        template< typename type_t >
        static void set(socket_base const& socket_in, type_t value_in) {
          option_setter< value_type_t >::set(socket_in, socket_option_t, value_in);
        }
    };

    template< socket_option socket_option_t, typename value_type_t >
    struct read_write_option: public read_option< socket_option_t, value_type_t > , public write_option<
        socket_option_t, value_type_t > {
        typedef value_type_t value_type;
    };

  } // namespace socket
} // namespace zmqh

#endif /* ZMQH_OPTION_HPP_ */
