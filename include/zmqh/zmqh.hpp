/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_ZMQH_HPP_
#define ZMQH_ZMQH_HPP_

#include <zmq.h>

namespace std {

  /**
   * @brief Placeholder for ::std::nullptr_t keyword.
   */
  // this is a const object...
  struct nullptr_t {
      // convertible to any type of null non-member pointer...
      template< class T >
      operator T *() const {
        return 0;
      }

      // or any type of null member pointer...
      template< class C, class T >
      operator T C::*() const {
        return 0;
      }

    private:
      // whose address can't be taken
      void operator &() const;
  };

  template< class T >
  bool operator==(T * lhs, nullptr_t const rhs) {
    return lhs == 0;
  }

  template< class T >
  bool operator==(nullptr_t const lhs, T * rhs) {
    return rhs == 0;
  }

  template< class T, class U >
  bool operator==(T U::* lhs, nullptr_t const rhs) {
    return lhs == 0;
  }

  template< class T, class U >
  bool operator==(nullptr_t const lhs, T U::* rhs) {
    return rhs == 0;
  }

} // namespace std

namespace {

  /**
   * @brief placeholder for nullptr keyword.
   */
  // and whose name is nullptr.
  const ::std::nullptr_t nullptr = { };

} // namespace

#endif /* ZMQH_ZMQH_HPP_ */
