/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_SOCKET_FWD_HPP_
#define ZMQH_SOCKET_FWD_HPP_

namespace zmqh {
  namespace socket {

    enum class type {
      PAIR = ZMQ_PAIR,
      PUB = ZMQ_PUB,
      SUB = ZMQ_SUB,
      REQ = ZMQ_REQ,
      REP = ZMQ_REP,
      XREQ = ZMQ_XREQ,
      XREP = ZMQ_XREP,
      PUSH = ZMQ_PUSH,
      PULL = ZMQ_PULL
    };

    enum class mode {
      BLOCK = 0, NO_BLOCK = ZMQ_NOBLOCK
    };

    struct socket_base;

    template< type socket_type_t >
    class socket;
    template< type socket_type_t >
    class read_only_socket;
    template< type socket_type_t >
    class write_only_socket;

    typedef socket< type::PAIR > pair;
    typedef write_only_socket< type::PUB > pub;
    typedef read_only_socket< type::SUB > sub;
    typedef socket< type::REQ > req;
    typedef socket< type::REP > rep;
    typedef socket< type::XREQ > xreq;
    typedef socket< type::XREP > xrep;
    typedef write_only_socket< type::PUSH > push;
    typedef read_only_socket< type::PULL > pull;

  } // namespace socket

  using socket::pair;
  using socket::pub;
  using socket::sub;
  using socket::req;
  using socket::rep;
  using socket::xreq;
  using socket::xrep;
  using socket::push;
  using socket::pull;

  using socket::mode;

} // namespace zmqh

#endif /* ZMQH_SOCKET_FWD_HPP_ */
