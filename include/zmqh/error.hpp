/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_ERROR_HPP_
#define ZMQH_ERROR_HPP_

#include "zmqh/zmqh.hpp"
#include <exception>

namespace zmqh {

  /**
   * @brief Wrapper for 0mq error as a standard exception.
   */
  class error_t: public ::std::exception {
    public:
      error_t() throw ();
      virtual char const* what() const throw ();

    private:
      int __errnum;
  };

} // namespace zmqh

#endif /* ZMQH_ERROR_HPP_ */
