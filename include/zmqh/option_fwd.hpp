/**
 * @file
 * @date 9 nov. 2010
 * @todo comment
 */

#ifndef ZMQH_OPTION_FWD_HPP_
#define ZMQH_OPTION_FWD_HPP_

#include "zmqh/zmqh.hpp"

namespace zmqh {
  namespace socket {

    enum class socket_option {
      // read-only
      RECEIVE_MORE = ZMQ_RCVMORE,
      // read-write
      HIGH_WATER_MARK_SIZE = ZMQ_HWM,
      // read-write
      SWAP_SIZE = ZMQ_SWAP,
      // read-write
      IO_THREADS_AFFINITY = ZMQ_AFFINITY,
      // read-write
      IDENTITY = ZMQ_IDENTITY,
      // write-only
      SUBSCRIBE = ZMQ_SUBSCRIBE,
      // write-only
      UNSUBSCRIBE = ZMQ_UNSUBSCRIBE,
      // read-write
      RATE = ZMQ_RATE,
      // read-write
      MULTICAST_RECOVERY_INTERVAL = ZMQ_RECOVERY_IVL,
      // read-write
      MULTICAST_LOOP = ZMQ_MCAST_LOOP,
      // read-write
      SEND_BUFFER_SIZE = ZMQ_SNDBUF,
      // read-write
      RECEIVE_BUFFER_SIZE = ZMQ_RCVBUF
    };

    template< typename value_type_t >
    struct option_setter;

    template< typename value_type_t >
    struct option_getter;

    template< socket_option socket_option_t, typename value_type_t >
    struct read_option;

    template< socket_option socket_option_t, typename value_type_t >
    struct write_option;

    template< socket_option socket_option_t, typename value_type_t >
    struct read_write_option;

    typedef read_option< socket_option::RECEIVE_MORE, bool > receive_more;
    typedef read_write_option< socket_option::HIGH_WATER_MARK_SIZE, uint64_t > high_water_mark_size;
    typedef read_write_option< socket_option::SWAP_SIZE, int64_t > swap_size;
    typedef read_write_option< socket_option::IO_THREADS_AFFINITY, uint64_t > io_threads_affinity;
    typedef read_write_option< socket_option::IDENTITY, binary_t > identity;
    typedef write_option< socket_option::SUBSCRIBE, binary_t > subscribe;
    typedef write_option< socket_option::UNSUBSCRIBE, binary_t > unsubscribe;
    typedef read_write_option< socket_option::RATE, int64_t > rate;
    typedef read_write_option< socket_option::MULTICAST_RECOVERY_INTERVAL, int64_t > multicast_recovery_interval;
    typedef read_write_option< socket_option::MULTICAST_LOOP, bool > multicast_loop;
    typedef read_write_option< socket_option::SEND_BUFFER_SIZE, uint64_t > send_buffer_size;
    typedef read_write_option< socket_option::RECEIVE_BUFFER_SIZE, uint64_t > receive_buffer_size;

  } // namespace socket

  using socket::receive_more;
  using socket::high_water_mark_size;
  using socket::swap_size;
  using socket::io_threads_affinity;
  using socket::identity;
  using socket::subscribe;
  using socket::unsubscribe;
  using socket::rate;
  using socket::multicast_recovery_interval;
  using socket::multicast_loop;
  using socket::send_buffer_size;
  using socket::receive_buffer_size;

} // namespace zmqh

#endif /* ZMQH_OPTION_FWD_HPP_ */
