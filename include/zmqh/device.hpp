/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_DEVICE_HPP_
#define ZMQH_DEVICE_HPP_

#include "zmqh/zmqh.hpp"
#include "zmqh/socket_fwd.hpp"

namespace zmqh {
  namespace device {

    enum class device_type {
      STREAMER = ZMQ_STREAMER, FORWARDER = ZMQ_FORWARDER, QUEUE = ZMQ_QUEUE
    };

    struct device_base {
      protected:
        void operator()(device_type const device_type_in,
                        socket::socket_base const& input_socket_in,
                        socket::socket_base const& output_socket_in);
    };

    template< device_type device_type_t >
    struct device: public device_base {
        void operator()(socket::socket_base const& input_socket_in, socket::socket_base const& output_socket_in) {
          device_base::operator ()(device_type_t, input_socket_in, output_socket_in);
        }
    };

    typedef device< device_type::STREAMER > streamer_t;
    typedef device< device_type::FORWARDER > forwarder_t;
    typedef device< device_type::QUEUE > queue_t;

    extern streamer_t streamer;
    extern forwarder_t forwarder;
    extern queue_t queue;

  }// namespace device

  using device::streamer;
  using device::forwarder;
  using device::queue;

} // namespace zmqh

#endif /* ZMQH_DEVICE_HPP_ */
