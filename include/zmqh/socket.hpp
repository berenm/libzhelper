/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_SOCKET_HPP_
#define ZMQH_SOCKET_HPP_

#include "zmqh/zmqh.hpp"
#include "zmqh/context.hpp"
#include "zmqh/binary.hpp"
#include "zmqh/message.hpp"

#include "zmqh/socket_fwd.hpp"
#include "zmqh/option_fwd.hpp"

#include <string>
#include <iostream>

#include <boost/shared_ptr.hpp>

namespace zmqh {
  struct multipart;

  namespace socket {

    struct socket_base {
        operator void*() const;

        template< typename option_type_t, typename value_type_t = typename option_type_t::value_type >
        void set_option(value_type_t const value_in) {
          option_type_t::set(*this, value_in);
        }

        template< typename option_type_t, typename value_type_t = typename option_type_t::value_type >
        value_type_t option() {
          return option_type_t::get(*this);
        }

        void set_id();

        void bind(::std::string const& address_in) throw (::zmqh::error_t);
        void connect(::std::string const& address_in) throw (::zmqh::error_t);

      protected:
        socket_base();
        socket_base(void const* socket_ptr_in);
        ~socket_base() throw (::zmqh::error_t);

        void const* const __socket_ptr;

        socket_base(socket_base const&);
        void operator =(socket_base const&);

        bool recv(mode const mode_in, message_t& message_out) throw (::zmqh::error_t);
        bool send(mode const mode_in, message_t const& message_out, bool const send_more_in) throw (::zmqh::error_t);
        void dump() throw (::zmqh::error_t);

        void set_block(bool block_in) {
          __block = block_in;
        }

        bool block() {
          return __block;
        }

        friend struct read_socket;
        friend struct write_socket;

      private:
        bool __block;
    };

    struct read_socket: public virtual socket_base {
        template< mode mode_t = mode::BLOCK >
        bool recv(message_t& message_out) throw (::zmqh::error_t) {
          return socket_base::recv(mode_t, message_out);
        }

        template< mode mode_t = mode::BLOCK >
        bool recv() throw (::zmqh::error_t) {
          message_t message;
          return recv< mode_t > (message);
        }

        template< mode mode_t = mode::BLOCK, typename data_t = binary_t >
        bool recv(data_t& data_out) {
          message_t message;
          bool result_out = recv< mode_t > (message);
          data_out = message.data< data_t > ();
          return result_out;
        }

        void dump() throw (::zmqh::error_t) {
          socket_base::dump();
        }

        void set_block(bool block_in) {
          socket_base::set_block(block_in);
        }

        bool block() {
          return socket_base::block();
        }
    };

    struct write_socket: public virtual socket_base {
        template< mode mode_t = mode::BLOCK >
        bool send(message_t const& message_in, bool const send_more_in = false) throw (::zmqh::error_t) {
          return socket_base::send(mode_t, message_in, send_more_in);
        }

        template< mode mode_t = mode::BLOCK >
        bool send() {
          return send< mode_t > (message_t());
        }
        template< mode mode_t = mode::BLOCK >
        bool send_more() {
          return send< mode_t > (message_t(), true);
        }

        template< mode mode_t = mode::BLOCK, typename data_t >
        bool send(data_t const& data_in) {
          return send< mode_t > (message_t(data_in));
        }
        template< mode mode_t = mode::BLOCK, typename data_t >
        bool send_more(data_t const& data_in) {
          return send< mode_t > (message_t(data_in), true);
        }

      protected:
        void set_more(bool more_in) {
          __more = more_in;
        }

        bool more() {
          return __more;
        }

        write_socket() :
          __more(false) {
        }

        template< typename data_t >
        void pend(data_t const& data_in) {
          flush();
          __pending_message.reset(new message_t(data_in));
        }
        void pend() {
          flush();
          __pending_message.reset(new message_t());
        }

        void flush() {
          if (__pending_message.get() == nullptr) {
            return;
          }

          socket_base::send(socket_base::block() ? mode::BLOCK : mode::NO_BLOCK, *__pending_message, __more);
          __pending_message.reset();
        }

        template< typename data_t >
        friend write_socket& operator<<(write_socket& socket_inout, data_t const& data_in);

      private:
        bool __more;
        ::boost::shared_ptr< message_t > __pending_message;
    };

    template< type socket_type_t >
    class socket: public read_socket, public write_socket, public virtual socket_base {
      public:
        explicit socket(context_t const& context_in = context_t::default_) throw (::zmqh::error_t) :
          socket_base(zmq_socket(context_in, static_cast< int > (socket_type_t))) {
          if (__socket_ptr == nullptr) {
            throw ::zmqh::error_t();
          }
        }
    };

    template< type socket_type_t >
    class read_only_socket: public read_socket, public virtual socket_base {
      public:
        explicit read_only_socket(context_t const& context_in = context_t::default_) throw (::zmqh::error_t) :
          socket_base(zmq_socket(context_in, static_cast< int > (socket_type_t))) {
          if (__socket_ptr == nullptr) {
            throw ::zmqh::error_t();
          }
        }
    };

    template< type socket_type_t >
    class write_only_socket: public write_socket, public virtual socket_base {
      public:
        explicit write_only_socket(context_t const& context_in = context_t::default_) throw (::zmqh::error_t) :
          socket_base(zmq_socket(context_in, static_cast< int > (socket_type_t))) {
          if (__socket_ptr == nullptr) {
            throw ::zmqh::error_t();
          }
        }
    };

  } // namespace socket
} // namespace zmqh

#endif /* ZMQH_SOCKET_HPP_ */
