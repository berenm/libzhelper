/**
 * @file
 * @date 16 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_BINARY_HPP_
#define ZMQH_BINARY_HPP_

#include <vector>

#include <boost/integer.hpp>
#include <boost/regex/pending/unicode_iterator.hpp>

namespace zmqh {

  typedef ::std::basic_string< ::boost::uint8_t > binary_t;
  typedef ::std::wstring string_t;

  binary_t to_binary(string_t const& string_in);
  binary_t to_binary(::std::string const& string_in);
  string_t to_string(binary_t const& binary_in);

} // namespace zmqh

#endif /* ZMQH_BINARY_HPP_ */
