/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_SOCKET_STREAM_HPP_
#define ZMQH_SOCKET_STREAM_HPP_

#include "zmqh/socket.hpp"

namespace zmqh {
  namespace socket {

    struct begm_type {
    };
    struct endm_type {
    };
    struct empty_type {
    };
    struct no_block_type {
    };
    struct block_type {
    };

    extern begm_type begm;
    extern endm_type endm;
    extern empty_type empty;
    extern no_block_type no_block;
    extern block_type block;

    template< typename data_t >
    write_socket& operator<<(write_socket& socket_inout, data_t const& data_in) {
      if (socket_inout.more()) {
        socket_inout.pend(data_in);
      } else {
        socket_inout.send(data_in);
      }

      return socket_inout;
    }

    template< >
    write_socket& operator<<< begm_type > (write_socket& socket_inout, begm_type const&);

    template< >
    write_socket& operator<<< endm_type > (write_socket& socket_inout, endm_type const&);

    template< >
    write_socket& operator<<< empty_type > (write_socket& socket_inout, empty_type const&);

    template< >
    write_socket& operator<<< no_block_type > (write_socket& socket_inout, no_block_type const&);

    template< >
    write_socket& operator<<< block_type > (write_socket& socket_inout, block_type const&);

    template< typename data_t >
    read_socket& operator>>(read_socket& socket_inout, data_t& data_out) {
      if (socket_inout.block()) {
        socket_inout.recv(data_out);
      } else {
        socket_inout.recv< mode::NO_BLOCK > (data_out);
      }

      return socket_inout;
    }

    template< >
    read_socket& operator>>< empty_type > (read_socket& socket_inout, empty_type&);

    template< >
    read_socket& operator>>< no_block_type > (read_socket& socket_inout, no_block_type&);

    template< >
    read_socket& operator>>< block_type > (read_socket& socket_inout, block_type&);

  } // namespace socket

  using socket::begm;
  using socket::endm;
  using socket::empty;
  using socket::no_block;
  using socket::block;

} // namespace zmqh

#endif /* ZMQH_SOCKET_STREAM_HPP_ */
