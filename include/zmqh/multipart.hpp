/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_MULTIPART_HPP_
#define ZMQH_MULTIPART_HPP_

#include "zmqh/zmqh.hpp"
#include "zmqh/message.hpp"
#include "zmqh/binary.hpp"

namespace zmqh {

  struct multipart {
      void add_address(binary_t const& address_in);
      void add_address(string_t const& address_in);

      void add_body(binary_t const& body_in);
      void add_body(string_t const& body_in);

      ::std::vector< binary_t > const& addresses() const;
      ::std::vector< binary_t > const& bodies() const;

    private:
      ::std::vector< binary_t > addresses_;
      ::std::vector< binary_t > bodies_;
  };

} // namespace zmqh

#endif /* ZMQH_MULTIPART_HPP_ */
