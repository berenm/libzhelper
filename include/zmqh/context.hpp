/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_CONTEXT_HPP_
#define ZMQH_CONTEXT_HPP_

#include "zmqh/zmqh.hpp"
#include "zmqh/error.hpp"

namespace zmqh {

  /**
   * @brief Wrapper for 0mq context.
   *
   * Default context offered as static member, with one I/O thread.
   */
  struct context_t {
      /** Default context, with one I/O thread. */
      static context_t default_;

      explicit context_t(int const io_threads_count_in) throw (::zmqh::error_t);
      ~context_t() throw (::zmqh::error_t);

      operator void*() const;

    private:
      void const* const __context_ptr;

      /** Non-copyable. Private copy constructor. */
      context_t(context_t const&);
      /** Non-assignable. Private = operator. */
      context_t& operator =(context_t const&);
  };

} // namespace zmqh

#endif /* ZMQH_CONTEXT_HPP_ */
