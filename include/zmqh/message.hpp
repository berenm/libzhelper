/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#ifndef ZMQH_MESSAGE_HPP_
#define ZMQH_MESSAGE_HPP_

#include "zmqh/zmqh.hpp"
#include "zmqh/binary.hpp"
#include "zmqh/socket_fwd.hpp"
#include "zmqh/error.hpp"

namespace zmqh {

  typedef zmq_free_fn free_fn;

  class message_t: private zmq_msg_t {
      friend class socket::socket_base;

    public:
      message_t() throw (::zmqh::error_t);
      explicit message_t(size_t const size_) throw (::zmqh::error_t);
      message_t(binary_t const& data_in) throw (::zmqh::error_t);
      message_t(string_t const& string_in) throw (::zmqh::error_t);
      ~message_t() throw (::zmqh::error_t);

      //      void rebuild()  throw(::zmqh::error_t);
      //      void rebuild(size_t size_)  throw(::zmqh::error_t);
      //      void rebuild(void* data_, size_t size_, free_fn* ffn_, void* hint_ = nullptr)  throw(::zmqh::error_t);

      //      void move(message_t& msg_)  throw(::zmqh::error_t);
      //      void copy(message_t const& msg_)  throw(::zmqh::error_t);

      template< typename data_t >
      void set_data(data_t const& data_in) throw (::zmqh::error_t) {
        set_data(binary_t(reinterpret_cast< binary_t::value_type const* > (&data_in), sizeof(data_in)));
      }

      template< typename data_t = binary_t >
      data_t data() const {
        data_t data_out;
        memcpy(&data_out, data().data(), ::std::min(sizeof(data_out), size()));

        return data_out;
      }

      size_t size() const;

    private:
      //  Disable implicit message copying, so that users won't use shared
      //  messages (less efficient) without being aware of the fact.
      message_t(message_t const& copy_in);
      void operator=(message_t const& copy_in);
  };

  template< >
  void message_t::set_data< binary_t >(binary_t const& data_in) throw (::zmqh::error_t);
  template< >
  void message_t::set_data< string_t >(string_t const& data_in) throw (::zmqh::error_t);

  template< >
  binary_t message_t::data< binary_t >() const;
  template< >
  string_t message_t::data< string_t >() const;

} // namespace zmqh

#endif /* ZMQH_MESSAGE_HPP_ */
