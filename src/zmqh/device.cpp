/**
 * @file
 * @date 13 nov. 2010
 * @todo comment
 */

#include "zmqh/device.hpp"
#include "zmqh/socket.hpp"
#include "zmqh/error.hpp"

namespace zmqh {
  namespace device {

    streamer_t streamer;
    forwarder_t forwarder;
    queue_t queue;

    void device_base::operator()(device_type const device_type_in,
                                 socket::socket_base const& input_socket_in,
                                 socket::socket_base const& output_socket_in) {
      int rc = zmq_device(static_cast< int > (device_type_in), input_socket_in, output_socket_in);
      if (rc != 0) {
        throw ::zmqh::error_t();
      }
    }

  } // namespace device
} // namespace zmqh
