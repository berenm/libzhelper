/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#include "zmqh/message.hpp"

#include <string>
#include <algorithm>
#include <iomanip>
#include <memory.h>

namespace zmqh {

  message_t::message_t() throw (::zmqh::error_t) {
    if (zmq_msg_init(this) != 0) {
      throw ::zmqh::error_t();
    }
  }

  message_t::message_t(const size_t size_) throw (::zmqh::error_t) {
    if (zmq_msg_init_size(this, size_) != 0) {
      throw ::zmqh::error_t();
    }
  }

  message_t::message_t(const binary_t& data_in) throw (::zmqh::error_t) {
    set_data(data_in);
  }

  message_t::message_t(const string_t& string_in) throw (::zmqh::error_t) {
    set_data(string_in);
  }

  message_t::~message_t() throw (::zmqh::error_t) {
    if (zmq_msg_close(this) != 0) {
      throw ::zmqh::error_t();
    }
  }

  //  void message_t::rebuild() throw (::zmqh::error_t) {
  //    if (zmq_msg_close(this) != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //
  //    if (zmq_msg_init(this) != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //  }
  //
  //  void message_t::rebuild(size_t size_) throw (::zmqh::error_t) {
  //    if (zmq_msg_close(this) != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //
  //    if (zmq_msg_init_size(this, size_) != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //  }
  //
  //  void message_t::rebuild(void* data_, size_t size_, free_fn *ffn_, void* hint_) throw (::zmqh::error_t) {
  //    if (zmq_msg_close(this) != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //
  //    if (zmq_msg_init_data(this, data_, size_, ffn_, hint_) != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //  }

  //  void message_t::move(message_t& msg_) throw (::zmqh::error_t) {
  //    if (zmq_msg_move(this, reinterpret_cast< zmq_msg_t* > (&msg_)) != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //  }
  //
  //  void message_t::copy(message_t const& msg_) throw (::zmqh::error_t) {
  //    if (zmq_msg_copy(this,
  //        const_cast< zmq_msg_t* > (reinterpret_cast< const zmq_msg_t* > (&msg_)))
  //        != 0) {
  //      throw ::zmqh::error_t();
  //    }
  //  }

  size_t message_t::size() const {
    return zmq_msg_size(const_cast< message_t* > (this));
  }

  template< >
  void message_t::set_data< binary_t >(binary_t const& data_in)
      throw (::zmqh::error_t) {
    if (zmq_msg_init_size(this, data_in.size()) != 0) {
      throw ::zmqh::error_t();
    }

    memcpy(zmq_msg_data(this), data_in.data(), data_in.size());
  }

  template< >
  void message_t::set_data< string_t >(string_t const& data_in)
      throw (::zmqh::error_t) {
    set_data(to_binary(data_in));
  }

  template< >
  binary_t message_t::data< binary_t >() const {
    return binary_t(reinterpret_cast< binary_t::value_type* > (zmq_msg_data(
        const_cast< message_t* > (this))), size());
  }

  struct is_print: ::std::unary_function< binary_t::value_type, bool > {
      bool operator()(const binary_t::value_type char_in) const {
        return ::std::isprint(char_in);
      }
  };

  struct hexa_printer: ::std::unary_function< binary_t::value_type, void > {
      hexa_printer(::std::wostringstream& stream_in) :
        stream_(stream_in) {
      }

      void operator()(const binary_t::value_type char_in) {
        stream_ << ::std::hex << ::std::setw(2) << ::std::setfill(L'0')
            << (char_in & 0xFF);
      }

    private:
      ::std::wostringstream& stream_;
  };

  template< >
  string_t message_t::data< string_t >() const {
    binary_t const& binary = data();
    binary_t::const_iterator last = binary.end() - 1;

    if (::std::find_if(binary.begin(), last, ::std::not1(is_print())) == last) {
      return to_string(data());
    } else {
      ::std::wostringstream stream;
      ::std::for_each(binary.begin(), binary.end(), hexa_printer(stream));
      return stream.str();
    }
  }

} // namespace zmqh
