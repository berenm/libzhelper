/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#include "zmqh/error.hpp"

namespace zmqh {

  error_t::error_t() throw () :
    __errnum(zmq_errno()) {
  }

  const char* error_t::what() const throw () {
    return zmq_strerror(__errnum);
  }

} // namespace zmqh
