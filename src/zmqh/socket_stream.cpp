/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#include "zmqh/socket_stream.hpp"

namespace zmqh {
  namespace socket {

    template< >
    write_socket& operator<<< begm_type > (write_socket& socket_inout, begm_type const&) {
      socket_inout.set_more(true);
      return socket_inout;
    }

    template< >
    write_socket& operator<<< endm_type > (write_socket& socket_inout, endm_type const&) {
      socket_inout.set_more(false);
      socket_inout.flush();
      return socket_inout;
    }

    template< >
    write_socket& operator<<< empty_type > (write_socket& socket_inout, empty_type const&) {
      if (socket_inout.more()) {
        socket_inout.pend();
      } else {
        socket_inout.send();
      }

      return socket_inout;
    }

    template< >
    write_socket& operator<<< no_block_type > (write_socket& socket_inout, no_block_type const&) {
      socket_inout.set_block(false);
      return socket_inout;
    }

    template< >
    write_socket& operator<<< block_type > (write_socket& socket_inout, block_type const&) {
      socket_inout.set_block(true);
      return socket_inout;
    }

    template< >
    read_socket& operator>>< empty_type > (read_socket& socket_inout, empty_type&) {
      if (socket_inout.block()) {
        socket_inout.recv();
      } else {
        socket_inout.recv< mode::NO_BLOCK > ();
      }

      return socket_inout;
    }

    template< >
    read_socket& operator>>< no_block_type > (read_socket& socket_inout, no_block_type&) {
      socket_inout.set_block(false);
      return socket_inout;
    }

    template< >
    read_socket& operator>>< block_type > (read_socket& socket_inout, block_type&) {
      socket_inout.set_block(true);
      return socket_inout;
    }

    begm_type begm;
    endm_type endm;
    empty_type empty;
    no_block_type no_block;
    block_type block;

  } // namespace socket
} // namespace zmqh
