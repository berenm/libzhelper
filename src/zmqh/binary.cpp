/**
 * @file
 * @date 16 oct. 2010
 * @todo comment
 */

#include "zmqh/binary.hpp"

namespace zmqh {

  binary_t to_binary(string_t const& string_in) {
    typedef ::boost::u32_to_u8_iterator< ::std::wstring::const_iterator > to_utf8;

    return binary_t(to_utf8(string_in.begin()), to_utf8(string_in.end()));// + static_cast< binary_t::value_type > ('\0');
  }

  binary_t to_binary(::std::string const& string_in) {
    return binary_t(string_in.begin(), string_in.end());// + static_cast< binary_t::value_type > ('\0');
  }

  string_t to_string(binary_t const& binary_in) {
    typedef ::boost::u8_to_u32_iterator< binary_t::const_iterator > to_utf32;

    if (binary_in.size() > 0) {
      return string_t(to_utf32(binary_in.begin()), to_utf32(binary_in.end()));// - 1));
    } else {
      return string_t();
    }
  }

} // namespace zmqh
