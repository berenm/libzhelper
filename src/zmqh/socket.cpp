/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#include "zmqh/socket.hpp"
#include "zmqh/context.hpp"
#include "zmqh/message.hpp"
#include "zmqh/multipart.hpp"
#include "zmqh/option.hpp"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>

namespace zmqh {
  namespace socket {

    socket_base::socket_base(void const* socket_ptr_in) :
      __socket_ptr(socket_ptr_in), __block(true) {
    }

    socket_base::~socket_base() throw (::zmqh::error_t) {
      if (zmq_close(*this) != 0) {
        throw ::zmqh::error_t();
      }
    }

    socket_base::operator void*() const {
      return const_cast< void* > (__socket_ptr);
    }

    //    void socket_base::setsockopt(int option_, const void* optval_,
    //        size_t optvallen_)  throw(::zmqh::error_t) {
    //      int rc = zmq_setsockopt(*this, option_, optval_, optvallen_);
    //      if (rc != 0) {
    //        throw ::zmqh::error_t();
    //      }
    //    }
    //
    //    void socket_base::getsockopt(int option_, void* optval_, size_t *optvallen_)  throw(::zmqh::error_t) {
    //      int rc = zmq_getsockopt(*this, option_, optval_, optvallen_);
    //      if (rc != 0) {
    //        throw ::zmqh::error_t();
    //      }
    //    }

    void socket_base::set_id() {
      ::std::ostringstream stream;
      stream << ::std::hex << ::std::setfill('0') << ::std::setw(4) << (int) ((float) (0x10000) * random() / (RAND_MAX
          + 1.0));
      stream << '-';
      stream << ::std::hex << ::std::setfill('0') << ::std::setw(4) << (int) ((float) (0x10000) * random() / (RAND_MAX
          + 1.0));

      set_option< ::zmqh::identity > (stream.str());
    }

    void socket_base::bind(const ::std::string& address_in) throw (::zmqh::error_t) {
      int rc = zmq_bind(*this, address_in.c_str());
      if (rc != 0) {
        throw ::zmqh::error_t();
      }
    }

    void socket_base::connect(const ::std::string& address_in) throw (::zmqh::error_t) {
      int rc = zmq_connect(*this, address_in.c_str());
      if (rc != 0) {
        throw ::zmqh::error_t();
      }
    }

    bool socket_base::recv(mode const mode_in, message_t& message_out) throw (::zmqh::error_t) {
      int rc = zmq_recv(*this, &message_out, static_cast< int > (mode_in));
      if (rc == 0) {
        return true;
      }

      if (mode_in == mode::NO_BLOCK && rc == -1 && zmq_errno() == EAGAIN) {
        return false;
      }

      throw ::zmqh::error_t();
    }

    bool socket_base::send(mode const mode_in, message_t const& message_in, bool const send_more_in = false)
        throw (::zmqh::error_t) {
      int flags = (send_more_in ? ZMQ_SNDMORE : 0);
      flags |= static_cast< int > (mode_in);

      int rc = zmq_send(*this, &const_cast< message_t& > (message_in), flags);
      if (rc == 0) {
        return true;
      }

      if (mode_in == mode::NO_BLOCK && rc == -1 && zmq_errno() == EAGAIN) {
        return false;
      }

      throw ::zmqh::error_t();
    }

    void socket_base::dump() throw (::zmqh::error_t) {
      do {
        message_t message;
        recv(mode::NO_BLOCK, message);
        ::std::wcout << message.data< string_t > () << ::std::endl;
      } while (socket_base::option< receive_more >());
    }

  //    struct part_sender: ::std::unary_function< binary_t, void > {
  //        part_sender(socket_base& socket_in) :
  //          socket_(socket_in) {
  //        }
  //
  //        void operator()(binary_t const& data_in) {
  //          socket_.send(message_t(data_in), ZMQ_SNDMORE);
  //        }
  //
  //        socket_base& socket_;
  //    };
  //
  //    template< >
  //    bool socket_base::send< multipart >(multipart const& multipart_in) {
  //      ::std::vector< binary_t > const& addresses = multipart_in.addresses();
  //      ::std::vector< binary_t > const& bodies = multipart_in.bodies();
  //
  //      ::std::for_each(addresses.begin(), addresses.end(), part_sender(*this));
  //      send();
  //      ::std::for_each(bodies.begin(), bodies.end() - 1, part_sender(*this));
  //      return send(*(bodies.end() - 1));
  //    }
  //
  //    template< >
  //    multipart socket_base::recv< multipart >() {
  //      multipart multipart;
  //
  //      bool receiving_addresses = true;
  //      do {
  //        binary_t binary = recv< binary_t > ();
  //
  //        if (binary.size() == 0) {
  //          receiving_addresses = false;
  //        } else {
  //          if (receiving_addresses) {
  //            multipart.add_address(binary);
  //          } else {
  //            multipart.add_body(binary);
  //          }
  //        }
  //      } while (option< receive_more > ());
  //
  //      return multipart;
  //    }

  } // namespace socket
} // namespace zmqh
