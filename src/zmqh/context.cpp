/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#include "zmqh/context.hpp"

namespace zmqh {

  context_t context_t::default_(1);

  context_t::context_t(const int io_threads_count_in = 1) throw (::zmqh::error_t) :
    __context_ptr(zmq_init(io_threads_count_in)) {
    if (__context_ptr == nullptr) {
      throw ::zmqh::error_t();
    }
  }

  context_t::~context_t() throw (::zmqh::error_t) {
    if (zmq_term(*this) != 0) {
      throw ::zmqh::error_t();
    }
  }

  context_t::operator void*() const {
    return const_cast< void* > (__context_ptr);
  }

} // namespace zmqh
