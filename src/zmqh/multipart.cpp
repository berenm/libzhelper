/**
 * @file
 * @date 13 oct. 2010
 * @todo comment
 */

#include "zmqh/multipart.hpp"
#include "zmqh/binary.hpp"

namespace zmqh {

  void multipart::add_address(binary_t const& address_in) {
    addresses_.push_back(address_in);
  }
  void multipart::add_address(string_t const& address_in) {
    addresses_.push_back(to_binary(address_in));
  }

  void multipart::add_body(binary_t const& body_in) {
    bodies_.push_back(body_in);
  }
  void multipart::add_body(string_t const& body_in) {
    bodies_.push_back(to_binary(body_in));
  }

  ::std::vector< binary_t > const& multipart::addresses() const {
    return addresses_;
  }
  ::std::vector< binary_t > const& multipart::bodies() const {
    return bodies_;
  }

} // namespace zmqh
